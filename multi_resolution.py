import csv as csv
import numpy as np
import time
import datetime as DT
from matplotlib.dates import date2num
from sklearn import metrics
from calendar import monthrange
from math import ceil



def date_fmt(y,m,d):
    if m<0:
        y = y-1
        m = 13+m
    return DT.date(y,m,min(monthrange(y, m)[1], d))


def week_of_month(dt):
    first_day = dt.replace(day=1)
    adjusted_dom = dt.day + first_day.weekday()
    w_in_m = int(ceil(adjusted_dom/7.0))
    w_in_y = w_in_m + first_day.isocalendar()[1]-1
    return w_in_m, w_in_y
# def week_of_month(year, month, day):
#     x = np.array(calendar.monthcalendar(year, month))
#     week_of_month = np.where(x==day)[0][0] + 1
#     return(week_of_month)

def extract_h(y,m,d,h,time_step):
    c_date = DT.datetime(y,m,d,hour=h)
    date_list = []
    for delta in range(time_step,0,-1):
        dd = c_date-DT.timedelta(hours = delta)
        time_v = round(date2num(dd), 3)
        date_list.append(time_v)
    return date_list

def extract_d_h(y,m,d,h,time_step):
    c_date = DT.datetime(y,m,d,hour=h)
    date_list = []
    for delta in range(time_step,0,-1):
        dd = c_date-DT.timedelta(days = delta)
        time_v = round(date2num(dd), 3)
        date_list.append(time_v)
    return date_list

def extract_d(y,m,d, time_step, is_int = True):
    c_date = DT.date(y,m,d)
    date_list = []
    for delta in range(time_step,0,-1):
        dd = c_date-DT.timedelta(days = delta)
        if is_int:
            date_list.append(int(date2num(dd)))
        else:
            date_list.append(date2num(dd))
        # print(delta, dd, date2num(dd))
    return date_list

def extract_w(y,m,d, time_step, is_int = True):
    c_date = DT.date(y,m,d)
    date_list = []
    for delta in range(time_step,0,-1):
        dd = c_date - DT.timedelta(weeks=delta)
        c_y = dd.isocalendar()[0]
        c_w = dd.isocalendar()[1]
        first_day = date_fmt(c_y, 1, 1); first_w_y = first_day.isocalendar()[2]
        bias = 0 if first_w_y == 1 else 1
        sub_date_list = []
        for dd in [1, 0]:#1 stands for Monday, 0 stands for Sunday
            myDate = str(c_y)+' '+str(c_w-bias)+' '+str(dd)
            f_v = DT.datetime.strptime(myDate, "%Y %W %w")
            if is_int:
                sub_date_list.append(int(date2num(f_v)))
            else:
                sub_date_list.append(date2num(f_v))
        date_list.append(sub_date_list)
        # print(delta, sub_date_list)
    return date_list

def extract_m(y,m,d, time_step, is_int = True):
    date_list = []
    for delta in range(time_step,0,-1):
        if m-delta>0:
            yy = y; mm = m-delta
        else:
            yy = y-1; mm = 12+(m-delta)
        c_d_range = monthrange(yy, mm)[1]
        if is_int:
            date_list.append([int(date2num(DT.date(yy,mm,1))), int(date2num(DT.date(yy,mm,c_d_range)))])
        else:
            date_list.append([date2num(DT.date(yy,mm,1)), date2num(DT.date(yy,mm,c_d_range))])
        # print(delta, [date2num(DT.date(yy,mm,1)), date2num(DT.date(yy,mm,c_d_range))])
    return date_list

def extract_w_h(y,m,d,h,time_step):
    c_date = DT.datetime(y,m,d,hour=h)
    date_list = []
    for delta in range(time_step,0,-1):
        dd = c_date - DT.timedelta(weeks=delta)
        time_v = round(date2num(dd), 3)
        date_list.append(time_v)
    return date_list

def extract_w_d(y,m,d, time_step, is_int = True):
    c_date = DT.date(y,m,d)
    date_list = []
    for delta in range(time_step,0,-1):
        dd = c_date - DT.timedelta(weeks=delta)
        if is_int:
            date_list.append(int(date2num(dd)))
        else:
            date_list.append(date2num(dd))
        # print(delta, dd, date2num(dd))
    return date_list

def extract_m_h(y,m,d,h,time_step):
    date_list = []
    for delta in range(time_step,0,-1):
        f_v = date_fmt(y, m-delta-1, d) if m-delta <= 0 else date_fmt(y, m-delta, d)
        y = f_v.year; m = f_v.month; d = f_v.day
        time_v = round(date2num(DT.datetime(y, m, d, hour=h)), 3)
        date_list.append(time_v)
    return date_list

def extract_m_d(y,m,d, time_step, is_int = True):
    date_list = []
    for delta in range(time_step,0,-1):
        f_v = date_fmt(y, m-delta-1, d) if m-delta <= 0 else date_fmt(y, m-delta, d)
        if is_int:
            date_list.append(int(date2num(f_v)))
        else:
            date_list.append(date2num(f_v))
        # print(delta, f_v, date2num(f_v))
    return date_list

def extract_y_d(y,m,d, time_step, is_int = True):
    date_list = []
    for delta in range(time_step,0,-1):
        f_v = date_fmt(y-delta, m, d)
        if is_int:
            date_list.append(int(date2num(f_v)))
        else:
            date_list.append(date2num(f_v))
        # print(delta, f_v, date2num(f_v))
    return date_list

def extract_m_w(y,m,d, time_step, is_int = True):
    date_list = []
    for delta in range(time_step,0,-1):
        if m-delta>0:
            yy = y; mm = m-delta
        else:
            yy = y-1; mm = 12+(m-delta)
        c_date = date_fmt(yy, mm, d)
        # w_m, w_y = week_of_month(c_date)
        w_y =  c_date.isocalendar()[1]
        first_day = date_fmt(yy, 1, 1); first_w_y = first_day.isocalendar()[1]; first_w_d = first_day.isocalendar()[2]
        bias = 1 if first_w_y==1 and first_w_d!=1 else 0
        sub_date_list = []
        for dd in [1, 0]:#1 stands for Monday, 0 stands for Sunday
                myDate = str(yy)+' '+str(w_y-bias)+' '+str(dd)
                f_v = DT.datetime.strptime(myDate, "%Y %W %w")
                if is_int:
                    sub_date_list.append(int(date2num(f_v)))
                else:
                    sub_date_list.append(date2num(f_v))
        # print(delta, [Date_date(_) for _ in sub_date_list])
        date_list.append(sub_date_list)
    return date_list

def extract_multi_resolution(y,m,d,time_step):#day, week, month
    multi_resolution_list = {}
    multi_resolution_list['d'] = extract_d(y,m,d,time_step)
    multi_resolution_list['w'] = extract_w(y,m,d,time_step)
    multi_resolution_list['w_d'] = extract_w_d(y,m,d,time_step)
    multi_resolution_list['m'] = extract_m(y,m,d,time_step)
    multi_resolution_list['m_d'] = extract_m_d(y,m,d,time_step)
    multi_resolution_list['m_w'] = extract_m_w(y,m,d,time_step)
    return multi_resolution_list

def extract_multi_mwdh(y,m,d,h,time_step):#day, week, month
    multi_resolution_list = {}
    multi_resolution_list['h'] = extract_h(y,m,d,h,time_step)
    multi_resolution_list['d'] = extract_d(y,m,d,time_step, is_int=False)
    multi_resolution_list['d_h'] = extract_d_h(y, m, d, h, time_step)
    multi_resolution_list['w'] = extract_w(y,m,d,time_step, is_int=False)
    multi_resolution_list['w_h'] = extract_w_h(y,m,d,h,time_step)
    multi_resolution_list['w_d'] = extract_w_d(y,m,d,time_step, is_int=False)
    multi_resolution_list['m'] = extract_m(y,m,d,time_step, is_int=False)
    multi_resolution_list['m_h'] = extract_m_h(y, m, d, h, time_step)
    multi_resolution_list['m_d'] = extract_m_d(y,m,d,time_step, is_int=False)
    multi_resolution_list['m_w'] = extract_m_w(y,m,d,time_step, is_int=False)
    return multi_resolution_list

def extract_date_mr(min_date, max_date, time_step):
    my_date_mr = {}
    for d in range(min_date, max_date+1):
        date_fv = Date_date(d)
        date_mr_list = extract_multi_resolution(date_fv.year, date_fv.month, date_fv.day,time_step)
        my_date_mr[d] = date_mr_list
    return my_date_mr

def extract_date_mh(min_date, max_date, time_step):
    my_date_mh = {}
    for d_ in range(min_date, max_date+1):
        for h in range(24):
            date_fv = Date_date(d_)
            y = date_fv.year; m = date_fv.month; d = date_fv.day
            time_v = round(date2num(DT.datetime(y,m,d, hour=h)),3)
            date_mr_list = extract_multi_mwdh(y, m, d, h, time_step)
            my_date_mh[time_v] = date_mr_list
    return my_date_mh

def extract_multiview_date_mr(min_date, max_date):
    my_date_mr = {}
    for d in range(min_date, max_date+1):
        date_fv = Date_date(d)
        #year, month, day, week number in month, week number in year, weekday
        date_mr_list = [date_fv.year, date_fv.month, date_fv.day]+[week_of_month(date_fv)[0], week_of_month(date_fv)[1]]+[date_fv.isocalendar()[2]]
        my_date_mr[d] = date_mr_list
    return my_date_mr

Date_date = lambda v: DT.datetime.fromordinal(int(v))
# my_date_mr = extract_date_mr(735234, 735598, 5)
# my_date_mr = extract_multiview_date_mr(735234, 735598)
# print(my_date_mr)


# print(_)
# for h in range(24):
#     print(DT.datetime(2011,3,1, hour=h), round(date2num(DT.datetime(2011,3,1, hour=h)),3))
    # out_f = extract_multi_mwdh(2011,3,1,h,4)
    # for k in out_f:
    #     print(k, out_f[k])
    # print(' ')
# my_date_mr = extract_date_mh(735234, 735236, 2)
# for i in range(1990, 2001):
#     extract_m_w(i,10,2, 5, is_int = True)
#     print(' ')
#
# for yy in range(1990, 2001):
#     first_day = date_fmt(yy, 1, 1);
#     first_w_y = first_day.isocalendar()
#     print(first_day, first_w_y)
#     extract_m_w(yy,10,2, 5, is_int = True)
# [1991, 1992, 1997, 1998]

# for i in range(24):
#     print(i, round(i/24,3), round(1-i/24,3))