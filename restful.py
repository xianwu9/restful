import csv as csv
import numpy as np
import time
import datetime as DT
from matplotlib.dates import date2num
import tensorflow as tf
from tensorflow.contrib.layers import xavier_initializer, batch_norm
from sklearn import metrics
import math
from multi_resolution import *
from calendar import monthrange
import sys
import os

tf.set_random_seed(42)
start = time.time()


########################################################
# load files
def load_data(file_name):
    csv_file_name = csv.reader(open(file_name, 'rU'), dialect='excel')
    header_file_name = next(csv_file_name)
    column_file_name = {}
    for h in header_file_name:
        column_file_name[h] = []
    for idx, row in enumerate(csv_file_name):
        for h, v in zip(header_file_name, row):
            column_file_name[h].append(v)
    return header_file_name, column_file_name


####################
# load ID
def load_ID(file_name):
    csv_file_name = csv.reader(open(file_name, 'rU'), dialect='excel')
    column_file_name = []
    for row in csv_file_name:
        for v in row:
            column_file_name.append(v)
    return column_file_name


####################
# load files
def load_data_txt(file_name):
    fdata = open(file_name, 'r')
    txt_file_name = [line.rstrip() for line in fdata]
    column_file_name = []
    for raw in txt_file_name:
        row = raw.strip('\r\n').split(' ')
        for v in row:
            column_file_name.append(v)
    return column_file_name


####################
def seperate_pn(mtx_outlier_region, min_d, max_d):
    pts = [];
    prs = [];
    pos = [];
    pys = []
    nts = [];
    nrs = [];
    nos = [];
    nys = []
    for o in mtx_outlier_region:
        for r in mtx_outlier_region[o]:
            for t in mtx_outlier_region[o][r]:
                if int(t) <= max_d and int(t) > min_d:
                    if mtx_outlier_region[o][r][t] == 1:
                        pts.append(t);
                        prs.append(r);
                        pos.append(o);
                        pys.append(1)
                    else:
                        nts.append(t);
                        nrs.append(r);
                        nos.append(o);
                        nys.append(0)
    return (pts, prs, pos, pys, nts, nrs, nos, nys)


def gen_batchs(mtx_outlier_region):
    (train_pts, train_prs, train_pos, train_pys, train_nts, train_nrs, train_nos, train_nys) = seperate_pn(
        mtx_outlier_region, train_begin, validation_start)
    rng_state = np.random.get_state()
    np.random.shuffle(train_nts);
    np.random.set_state(rng_state)
    np.random.shuffle(train_nrs);
    np.random.set_state(rng_state)
    np.random.shuffle(train_nos);
    np.random.set_state(rng_state)
    np.random.shuffle(train_nys)
    num_ps = len(train_pys)
    train_ts = train_pts + train_nts[:int(1.25 * num_ps)]
    train_rs = train_prs + train_nrs[:int(1.25 * num_ps)]
    train_os = train_pos + train_nos[:int(1.25 * num_ps)]
    train_ys = train_pys + train_nys[:int(1.25 * num_ps)]
    'shuffle'
    rng_state = np.random.get_state()
    np.random.shuffle(train_ts);
    np.random.set_state(rng_state)
    np.random.shuffle(train_rs);
    np.random.set_state(rng_state)
    np.random.shuffle(train_os);
    np.random.set_state(rng_state)
    np.random.shuffle(train_ys)
    xs_batches = len(train_ts) // batch_size
    for idx in range(xs_batches):
        idx_begin = idx * batch_size
        idx_end = (idx + 1) * batch_size
        yield (train_ts[idx_begin:idx_end], train_rs[idx_begin:idx_end], train_os[idx_begin:idx_end],
               train_ys[idx_begin:idx_end])


def gen_epochs(n, mtx_outlier_region):
    for i in range(n):
        yield (gen_batchs(mtx_outlier_region))


def convert2idx(words, offset):
    dictionary = {}
    count = list(set(words))
    for word in count:
        dictionary[word] = len(dictionary) + offset
    reverse_dictionary = dict(zip(dictionary.values(), dictionary.keys()))
    return dictionary, reverse_dictionary

def convert2didx(min_v, max_v, offset):
    dictionary = {}
    for word in range(min_v, max_v+1):
        dictionary[word] = len(dictionary) + offset
    reverse_dictionary = dict(zip(dictionary.values(), dictionary.keys()))
    return dictionary, reverse_dictionary


# def extract_feature(o, r, d):
#     sequence = {}
#     for k in my_date_mr[d]:
#         if 'd' in k:
#             sequence[k] = []
#             for dd in my_date_mr[d][k]:
#                 sequence[k].append(mtx_outlier_region[o][r][dd])
#         else:
#             sequence[k] = []
#             for sub_ds in my_date_mr[d][k]:
#                 if sub_ds[1]-sub_ds[0]==0:
#                     sequence[k].append(2)
#                 else:
#                     v = 0
#                     print(o, r, dd)
#                     for dd in range(sub_ds[0], sub_ds[1]):
#                         if mtx_outlier_region[o][r][dd] == 1:
#                             v = 1
#                             break
#                     sequence[k].append(v)
#     return sequence

def extract_feature(o, r, d):
    sequence = {}
    for k in my_date_mr[d]:
        if 'd' in k:
            sequence[k] = []
            for dd in my_date_mr[d][k]:
                try:
                    sequence[k].append(mtx_outlier_region[o][r][dd])
                except:
                    sequence[k].append(0)
        else:
            sequence[k] = []
            for sub_ds in my_date_mr[d][k]:
                if sub_ds[1]-sub_ds[0]==0:
                    sequence[k].append(2)
                else:
                    v = 0
                    for dd in range(sub_ds[0], sub_ds[1]):
                        try:
                            if mtx_outlier_region[o][r][dd] == 1:
                                v = 1
                                break
                        except:
                            v = 0
                    sequence[k].append(v)
    return sequence


# [d_idx]['d'/'w'/'m'/'w_d'/'m_d'/'m_w']

def vali_test(mtx_outlier_region, min_d, max_d):
    (pts, prs, pos, pys, nts, nrs, nos, nys) = seperate_pn(mtx_outlier_region, min_d, max_d)
    ts = pts + nts
    rs = prs + nrs
    os = pos + nos
    ys = pys + nys
    sequences = {k:[] for k in ['d','w','m','w_d','m_d','m_w']}
    for o, r, d in zip(os, rs, ts):
        sequence = extract_feature(o, r, d)
        for k in sequence:
            sequences[k].append(sequence[k])
    return (rs, os, sequences, ys)


def transfer_dict(mr_dict):
    nan_didx = dict_time['Nan']
    min_didx = min_date
    _mr_dict = {}
    for d in mr_dict:
        _d = dict_time[d]
        _mr_dict[_d] = {}
        for k in mr_dict[d]:
            if 'd' in k:
                _mr_dict[_d][k] = []
                for _ in mr_dict[d][k]:
                    try:
                        _mr_dict[_d][k].append(dict_time[_])
                    except:
                        _mr_dict[_d][k].append(nan_didx)
            else:
                _mr_dict[_d][k] = []
                for sub in mr_dict[d][k]:
                    _sub = []
                    for _ in sub:
                        try:
                            _sub.append(dict_time[_])
                        except:
                            _sub.append(min_didx)
                    _mr_dict[_d][k].append(_sub)
    return _mr_dict


def generate_mtx(column_urban):
    mtx_outlier_region = {}
    set_outlier = list(set(column_urban['outlier']));
    set_region = list(set(column_urban['region']));
    set_date = list(set(column_urban['date']))
    for o in set_outlier:
        mtx_outlier_region[o] = {}
        for r in set_region:
            mtx_outlier_region[o][r] = {}
            for d in set_date:
                mtx_outlier_region[o][r][d] = 0
    for o, r, d in zip(column_urban['outlier'], column_urban['region'], column_urban['date']):
        mtx_outlier_region[o][r][d] = 1

    d = dict_time['Nan']
    for o in set_outlier:
        for r in set_region:
            mtx_outlier_region[o][r][d] = 2
    return mtx_outlier_region


# def eval_rslt(y_pred, y_t):
#     TP = sum([1 for y1,y2 in zip(y_pred, y_t) if y1*y2==1])
#     FN = sum(y_t) - TP
#     FP = sum(y_pred) - TP
#     TN = len(y_pred) - TP - FN - FP
#     prec = TP/(TP+FP+0.1)
#     recall = TP/(TP+FN+0.1)
#     f1 = 2*TP/(2*TP+FN+FP+0.1)
#     acc = (TP+TN)/(TP+FN+FP+TN+0.1)
#     # print(' TP:'+str(TP)+' FP:'+str(FP)+' TN:'+str(TN)+' FN:'+str(FN))
#     return(round(float(prec),3), round(float(recall),3), round(float(f1),3), round(float(acc),3))

def macro_micro(TP_list, FP_list, TN_list, FN_list):
    Macro_prec = sum(TP_list) / (sum(TP_list) + sum(FP_list) + 0.1)
    Micro_prec = np.mean([tpp / (tpp + fpp + 0.1) for tpp, fnn, fpp in zip(TP_list, FN_list, FP_list)])
    Macro_recall = sum(TP_list) / (sum(TP_list) + sum(FN_list) + 0.1)
    Micro_recall = np.mean([tpp / (tpp + fnn + 0.1) for tpp, fnn, fpp in zip(TP_list, FN_list, FP_list)])
    Macro_F1 = 2 * sum(TP_list) / (2 * sum(TP_list) + sum(FN_list) + sum(FP_list) + 0.1)
    Micro_F1 = np.mean([2 * tpp / (2 * tpp + fnn + fpp + 0.1) for tpp, fnn, fpp in zip(TP_list, FN_list, FP_list)])
    Macro_acc = (sum(TN_list) + sum(TP_list)) / (sum(TN_list) + sum(TP_list) + sum(FN_list) + sum(FP_list))
    Micro_acc = np.mean(
        [(tnn + tpp) / (tnn + tpp + fnn + fpp) for tpp, fnn, fpp, tnn in zip(TP_list, FN_list, FP_list, TN_list)])
    print('&', round(Macro_prec, 3), '&', round(Micro_prec, 3), '&', round(Macro_recall, 3), '&',
          round(Micro_recall, 3), '&', round(Macro_F1, 3), '&', round(Micro_F1, 3), '&', round(Macro_acc, 3), '&',
          round(Micro_acc, 3), '\\')


def eval_rslt(y_pred, y_t):
    TP = sum([1 for y1, y2 in zip(y_pred, y_t) if y1 == 1 and y2 == 1])
    FN = sum(y_t) - TP
    FP = sum([1 for y1 in y_pred if y1 == 1]) - TP
    TN = len(y_pred) - TP - FN - FP
    prec = TP / (TP + FP + 0.1)
    recall = TP / (TP + FN + 0.1)
    f1 = 2 * TP / (2 * TP + FN + FP + 0.1)
    acc = (TP + TN) / (TP + FN + FP + TN + 0.1)
    # print(' TP:'+str(TP)+' FP:'+str(FP)+' TN:'+str(TN)+' FN:'+str(FN))
    # print('len(y_t)', len(y_t), 'TP+FN+FP+TN:', TP+FN+FP+TN, 'len(y_pred)', len(y_pred))
    return (
    (round(float(prec), 3), round(float(recall), 3), round(float(f1), 3), round(float(acc), 3)), (TP, FP, TN, FN))


def run_eval(rs, os, ts, ys):
    xs_batches = 1 + len(ys) // 4096;
    my_preds = []
    for idx in range(xs_batches):
        idx_begin = idx * 4096
        idx_end = min((idx + 1) * 4096, len(ys))
        feed_dict = {input_dseq: np.array(ts['d'][idx_begin:idx_end]), input_wseq: np.array(ts['w'][idx_begin:idx_end]), \
                     input_mseq: np.array(ts['m'][idx_begin:idx_end]), input_wdseq: np.array(ts['w_d'][idx_begin:idx_end]), \
                     input_mdseq: np.array(ts['m_d'][idx_begin:idx_end]), input_mwseq: np.array(ts['m_w'][idx_begin:idx_end]), \
                     input_ridx: np.array(rs).reshape(-1, 1), \
                     output_c: np.array(ys), input_oidx: np.array(t_os).reshape(-1, 1), \
                     inputs_row: idx_end-idx_begin, keep_rate: 1.0, phase: 0, penalty_rate: 0.0001, phase_RNN: 0}
        my_pred = sess.run(pred_y, feed_dict=feed_dict)
        my_preds = my_preds + list(my_pred)
    my_pred = my_preds
    bi_pred = []
    for _ in list(my_pred):
        if _ >= 0.5:
            bi_pred.append(1)
        else:
            bi_pred.append(0)
    (prec, recall, f1, acc), (TP, FP, TN, FN) = eval_rslt(bi_pred, ys)
    fpr, tpr, thresholds = metrics.roc_curve(np.array(ys), np.array(my_pred), pos_label=1)
    my_auc = metrics.auc(fpr, tpr)
    my_ap = metrics.average_precision_score(np.array(ys), np.array(my_pred))
    print('======>', 'prec', prec, 'recall', recall, 'f1', f1, 'acc', round(float(my_auc), 3), 'ap',
          round(float(my_ap), 3))
    return ([prec, recall, f1, acc, my_auc, my_ap], os, my_preds, ys)


def seperate_category(os, my_preds, ys):
    outlier = {};
    TP_list = [];
    FP_list = [];
    TN_list = [];
    FN_list = []
    for o, p, y in zip(os, my_preds, ys):
        if p >= 0.5:
            bi_v = 1
        else:
            bi_v = 0
        try:
            outlier[o]['my_pred'].append(p)
            outlier[o]['bi_pred'].append(bi_v)
            outlier[o]['ys'].append(y)
        except:
            outlier[o] = {}
            outlier[o]['my_pred'] = [p]
            outlier[o]['bi_pred'] = [bi_v]
            outlier[o]['ys'] = [y]
    for o in outlier:
        my_pred = outlier[o]['my_pred'];
        bi_pred = outlier[o]['bi_pred'];
        ys = outlier[o]['ys']
        (prec, recall, f1, acc), (TP, FP, TN, FN) = eval_rslt(bi_pred, ys)
        TP_list.append(TP);
        FP_list.append(FP);
        TN_list.append(TN);
        FN_list.append(FN)
        fpr, tpr, thresholds = metrics.roc_curve(np.array(ys), np.array(my_pred), pos_label=1)
        my_auc = metrics.auc(fpr, tpr)
        my_ap = metrics.average_precision_score(np.array(ys), np.array(my_pred))
        print(reverse_dict_outlier[o], ' &', prec, ' &', recall, ' &', f1, ' &', round(float(my_auc), 3), ' &',
              round(float(my_ap), 3))
    macro_micro(TP_list, FP_list, TN_list, FN_list)
    print(' ')


def attention(inputs, str_name, time_major=False):
    if isinstance(inputs, tuple):
        # In case of Bi-RNN, concatenate the forward and the backward RNN outputs.
        inputs = tf.concat(inputs, 2)

    if time_major:
        # (T,B,D) => (B,T,D)
        inputs = tf.transpose(inputs, [1, 0, 2])

    with tf.variable_scope('attention' + str_name):
        inputs_shape = inputs.shape
        sequence_length = inputs_shape[1].value  # the length of sequences processed in the antecedent RNN layer
        hidden_size = inputs_shape[2].value  # hidden size of the RNN layer

        # Attention mechanism
        W_omega = tf.Variable(tf.random_normal([hidden_size, attention_size], stddev=0.1))
        b_omega = tf.Variable(tf.random_normal([attention_size], stddev=0.1))
        u_omega = tf.Variable(tf.random_normal([attention_size], stddev=0.1))

        v = tf.tanh(tf.matmul(tf.reshape(inputs, [-1, hidden_size]), W_omega) + tf.reshape(b_omega, [1, -1]))
        vu = tf.matmul(v, tf.reshape(u_omega, [-1, 1]))
        exps = tf.reshape(tf.exp(vu), [-1, sequence_length])
        alphas = exps / tf.reshape(tf.reduce_sum(exps, 1), [-1, 1])

        output = tf.reduce_sum(inputs * tf.reshape(alphas, [-1, sequence_length, 1]), 1)

        return output


def my_LSTM(x1, inputs_row, str_name, phase_RNN):
    with tf.variable_scope('rnn' + str_name):
        'Placeholders'
        zero_state1 = tf.get_variable('zero_state1', [1, state_size], initializer=tf.constant_initializer(0.0))
        init_state1 = tf.matmul(tf.ones([inputs_row, 1]), zero_state1)
        zero_state2 = tf.get_variable('zero_state2', [1, state_size], initializer=tf.constant_initializer(0.0))
        init_state2 = tf.matmul(tf.ones([inputs_row, 1]), zero_state2)
        zero_state3 = tf.get_variable('zero_state3', [1, state_size], initializer=tf.constant_initializer(0.0))
        init_state3 = tf.matmul(tf.ones([inputs_row, 1]), zero_state3)
        zero_state4 = tf.get_variable('zero_state4', [1, state_size], initializer=tf.constant_initializer(0.0))
        init_state4 = tf.matmul(tf.ones([inputs_row, 1]), zero_state4)

        'RNN Inputs'
        x1_one_hot = tf.one_hot(x1, 3)  # tensor size: batch_size, num_steps, num_classes
        rnn1_inputs = tf.unstack(x1_one_hot, axis=1)  # unstack to num_steps of tensors: batch_size, num_classes

        'Definition of rnn_cell'
        WS = tf.get_variable('WS', [3 + state_size, state_size], initializer=tf.truncated_normal_initializer(stddev=0.1))
        bS = tf.get_variable('bS', [state_size], initializer=tf.constant_initializer(0.0))
        WI = tf.get_variable('WI', [3 + state_size, state_size], initializer=tf.truncated_normal_initializer(stddev=0.1))
        bI = tf.get_variable('bI', [state_size], initializer=tf.constant_initializer(0.0))
        WO = tf.get_variable('WO', [3 + state_size, state_size], initializer=tf.truncated_normal_initializer(stddev=0.1))
        bO = tf.get_variable('bO', [state_size], initializer=tf.constant_initializer(0.0))
        WF = tf.get_variable('WF', [3 + state_size, state_size], initializer=tf.truncated_normal_initializer(stddev=0.1))
        bF = tf.get_variable('bF', [state_size], initializer=tf.constant_initializer(0.0))

        def lstm_cell(rnn1_input, statec, stateh):
            input_t = tf.concat([rnn1_input, stateh], 1)
            igate = tf.sigmoid(tf.matmul(input_t, WI) + bI)
            ogate = tf.sigmoid(tf.matmul(input_t, WO) + bO)
            fgate = tf.sigmoid(tf.matmul(input_t, WF) + bF)
            statec_ = tf.tanh(tf.contrib.layers.batch_norm(tf.matmul(input_t, WS) + bS, scale=True, is_training=phase_RNN))
            statec = tf.multiply(fgate, statec) + tf.multiply(igate, tf.nn.dropout(statec_, keep_rate))
            stateh = tf.multiply(ogate, tf.tanh(statec))
            return statec, stateh

        statec = init_state1
        stateh = init_state2
        rnn_outputs = []
        for rnn1_input in rnn1_inputs:
            statec, stateh = lstm_cell(rnn1_input, statec, stateh)
            rnn_outputs.append(stateh)

        final_state = rnn_outputs[-1]
    return final_state

def my_RNN(x1, inputs_row, str_name, phase_RNN):
    with tf.variable_scope('rnn' + str_name):
        'Placeholders'
        zero_state1 = tf.get_variable('zero_state1', [1, state_size], initializer=tf.constant_initializer(0.0))
        init_state1 = tf.matmul(tf.ones([inputs_row, 1]), zero_state1)
        zero_state2 = tf.get_variable('zero_state2', [1, state_size], initializer=tf.constant_initializer(0.0))
        init_state2 = tf.matmul(tf.ones([inputs_row, 1]), zero_state2)
        zero_state3 = tf.get_variable('zero_state3', [1, state_size], initializer=tf.constant_initializer(0.0))
        init_state3 = tf.matmul(tf.ones([inputs_row, 1]), zero_state3)
        zero_state4 = tf.get_variable('zero_state4', [1, state_size], initializer=tf.constant_initializer(0.0))
        init_state4 = tf.matmul(tf.ones([inputs_row, 1]), zero_state4)

        'RNN Inputs'
        x1_one_hot = tf.one_hot(x1, 3)
        rnn1_inputs = tf.unstack(x1_one_hot, axis=1)  # unstack to num_steps of tensors: batch_size, num_classes

        'Definition of rnn_cell'
        WS = tf.get_variable('WS', [3 + state_size, state_size], initializer=tf.truncated_normal_initializer(stddev=0.1))
        bS = tf.get_variable('bS', [state_size], initializer=tf.constant_initializer(0.0))
        WO = tf.get_variable('WO', [state_size, state_size], initializer=tf.truncated_normal_initializer(stddev=0.1))
        bO = tf.get_variable('bO', [state_size], initializer=tf.constant_initializer(0.0))


        def lstm_cell(rnn1_input, statec, stateh):
            input_t = tf.concat([rnn1_input, statec], 1)
            statec = tf.sigmoid(tf.matmul(input_t, WS) + bS)
            stateh = tf.matmul(statec, WO)+bO
            return statec, stateh

        statec = init_state1
        stateh = init_state2
        rnn_outputs = []
        for rnn1_input in rnn1_inputs:
            statec, stateh = lstm_cell(rnn1_input, statec, stateh)
            rnn_outputs.append(stateh)

        final_state = rnn_outputs[-1]
    return final_state

def my_GRU(x1, inputs_row, str_name, phase_RNN):
    with tf.variable_scope('rnn' + str_name):
        'Placeholders'
        zero_state1 = tf.get_variable('zero_state1', [1, state_size], initializer=tf.constant_initializer(0.0))
        init_state1 = tf.matmul(tf.ones([inputs_row, 1]), zero_state1)
        zero_state2 = tf.get_variable('zero_state2', [1, state_size], initializer=tf.constant_initializer(0.0))
        init_state2 = tf.matmul(tf.ones([inputs_row, 1]), zero_state2)
        zero_state3 = tf.get_variable('zero_state3', [1, state_size], initializer=tf.constant_initializer(0.0))
        init_state3 = tf.matmul(tf.ones([inputs_row, 1]), zero_state3)
        zero_state4 = tf.get_variable('zero_state4', [1, state_size], initializer=tf.constant_initializer(0.0))
        init_state4 = tf.matmul(tf.ones([inputs_row, 1]), zero_state4)

        'RNN Inputs'
        x1_one_hot = tf.one_hot(x1, 3)
        rnn1_inputs = tf.unstack(x1_one_hot, axis=1)  # unstack to num_steps of tensors: batch_size, num_classes

        'Definition of rnn_cell'
        WS = tf.get_variable('WS', [3 + state_size, state_size], initializer=tf.truncated_normal_initializer(stddev=0.1))
        bS = tf.get_variable('bS', [state_size], initializer=tf.constant_initializer(0.0))
        WI = tf.get_variable('WI', [3 + state_size, state_size], initializer=tf.truncated_normal_initializer(stddev=0.1))
        bI = tf.get_variable('bI', [state_size], initializer=tf.constant_initializer(0.0))
        WO = tf.get_variable('WO', [3 + state_size, state_size], initializer=tf.truncated_normal_initializer(stddev=0.1))
        bO = tf.get_variable('bO', [state_size], initializer=tf.constant_initializer(0.0))


        def lstm_cell(rnn1_input, statec):
            input_t = tf.concat([rnn1_input, statec], 1)
            rgate = tf.sigmoid(tf.matmul(input_t, WI) + bI)
            zgate = tf.sigmoid(tf.matmul(input_t, WO) + bO)
            statec_ = tf.tanh(tf.matmul(tf.concat([rnn1_input, tf.multiply(rgate, statec)], 1), WS) + bS)
            statec = tf.multiply(zgate, statec) + tf.multiply(1-zgate, statec_)
            return statec

        statec = init_state1
        rnn_outputs = []
        for rnn1_input in rnn1_inputs:
            statec = lstm_cell(rnn1_input, statec)
            rnn_outputs.append(statec)

        final_state = rnn_outputs[-1]
    return final_state

def MLP(inputs, input_dim, output_dim, str_name):
    with tf.variable_scope('mlp' + str_name):
        W_mlp = tf.get_variable('W_mlp', [input_dim, output_dim],
                                initializer=tf.truncated_normal_initializer(stddev=0.1))
        b_mlp = tf.get_variable('b_mlp', [output_dim], initializer=tf.constant_initializer(0.0))
        output = tf.matmul(inputs, W_mlp) + b_mlp
    return output


def CNN(current_input, inputs_row, is_train, keep_prob_cnn, str_name, device='/cpu:0'):
    if len(current_input.get_shape()) != 4:
        current_input = tf.expand_dims(current_input, axis=2)
    with tf.variable_scope('cnn' + str_name):
        with tf.device(device):
            cw_weights = {}; cw_biases = {}
            n_input = state_size
            for layer_i in range(n_filter_layer):
                n_output = int(n_input/2)
                cw_weights['e_' + str(layer_i)] = tf.get_variable('cw_weights/e_' + str(layer_i),
                                                                        [filter_sizes[layer_i], filter_sizes[layer_i], n_input, n_output],
                                                                        dtype=tf.float32,
                                                                        initializer=xavier_initializer())
                cw_biases['e_' + str(layer_i)] = tf.get_variable('cw_biases/e_' + str(layer_i),
                                                                       [n_output],
                                                                       dtype = tf.float32,
                                                                       initializer=tf.constant_initializer(0.0))

                output = tf.nn.relu(tf.add(
                    tf.nn.conv2d(current_input, cw_weights['e_' + str(layer_i)], strides=[1, 1, 1, 1], padding='SAME'),
                    cw_biases['e_' + str(layer_i)]))
                output = batch_norm(output,
                                    center=True,
                                    scale=True,
                                    is_training=is_train,
                                    trainable=True,
                                    scope='eclayer_%d_bn' % layer_i,
                                    decay=0.9)
                output = tf.cond(is_train,
                                 lambda: tf.nn.dropout(output, keep_prob=keep_prob_cnn),
                                 lambda: output)
                # output = tf.nn.max_pool(output, ksize=[1, 2, 1, 1], strides=[1, 2, 1, 1], padding='SAME', name="pool")
                n_input = n_output
                current_input = output
            # current_input = tf.reduce_mean(current_input, axis = 2)
            current_input = tf.reshape(current_input,[inputs_row, -1])
        return current_input

'******************************************************************'
num_steps = int(sys.argv[2])
Date_date = lambda v: DT.datetime.fromordinal(int(v))
DateFormat = lambda v: DT.datetime.fromordinal(int(v)).strftime("%Y-%m-%d")
Date2v = lambda date: date2num(DT.datetime.strptime(date, "%m/%d/%Y %H:%M"))
Date2month = lambda date: int(date[:4]) * 12 + int(date[5:7])
'*******************************************************************'
header_urban, column_urban = load_data('file.csv')
for h in column_urban:
    print(h, len(column_urban[h]), len(list(set(column_urban[h]))))
for index, i in enumerate(column_urban['outlier']):
    if "Noise" in i:
        column_urban['outlier'][index] = "Noise"

urban_date_day = [int(Date2v(u)) for u in column_urban['date']]
sort_urban_date_day = list(sorted(set(urban_date_day)))
column_urban['date'] = urban_date_day

# bm = int(sys.argv[1]);
# train_begin = int(Date2v(str(bm) + '/01/2017 00:01'))
# validation_start = int(Date2v(str(bm + 6) + '/14/2017 00:01'));
# validation_end = int(Date2v(str(bm + 6) + '/30/2017 00:01'));
# test_end = int(Date2v(str(bm + 7) + '/30/2017 00:01'))

# bm = int(sys.argv[1]);
# train_begin = int(Date2v(str(bm)+'/01/2017 00:01'))
# validation_start = int(Date2v(str(bm)+'/24/2017 00:01'));
# validation_end = int(Date2v(str(bm)+'/25/2017 00:01'));
# test_end = int(Date2v(str(bm)+'/30/2017 00:01'))

bm = int(sys.argv[1]);
train_begin = int(Date2v(str(bm) + '/01/2017 00:01'))
validation_start = int(Date2v(str(bm+4) + '/01/2017 00:01'));
validation_end = int(Date2v(str(bm+4) + '/30/2017 00:01'));
test_end = int(Date2v(str(bm + 5) + '/30/2017 00:01'))

# bm = int(sys.argv[1]);
# train_begin = int(Date2v(str(bm) + '/01/2017 00:01'))
# validation_start = int(Date2v(str(bm+6) + '/01/2017 00:01'));
# validation_end = int(Date2v(str(bm+6) + '/30/2017 00:01'));
# test_end = int(Date2v(str(bm+7) + '/30/2017 00:01'))

dict_outlier, reverse_dict_outlier = convert2idx(column_urban['outlier'], 0)
dict_region, reverse_dict_region = convert2idx(column_urban['region'], 0)
dict_time, reverse_dict_time = convert2didx(sort_urban_date_day[0], sort_urban_date_day[-1], 1)
dict_time['Nan'] = 0; reverse_dict_time[dict_time['Nan']] = 'Nan'

column_urban['region'] = [dict_region[r] for r in column_urban['region']]
column_urban['outlier'] = [dict_outlier[o] for o in column_urban['outlier']]
column_urban['date'] = [dict_time[d] for d in column_urban['date']]
train_begin = dict_time[train_begin]
validation_start = dict_time[validation_start]
validation_end = dict_time[validation_end]
test_end = dict_time[test_end]
min_date = dict_time[sort_urban_date_day[0]]

print(sort_urban_date_day[0], sort_urban_date_day[-1])
my_date_mr = extract_date_mr(sort_urban_date_day[0], sort_urban_date_day[-1], num_steps)
my_date_mr = transfer_dict(my_date_mr)# [d_idx]['d'/'w'/'m'/'w_d'/'m_d'/'m_w']

mtx_outlier_region = generate_mtx(column_urban)

(v_rs, v_os, v_ts, v_ys) = vali_test(mtx_outlier_region, validation_start, validation_end)
(t_rs, t_os, t_ts, t_ys) = vali_test(mtx_outlier_region, validation_end, test_end)

#feature = extract_feature(1, 1, 0)

# for _, lst in enumerate(v_ts['w']):
#     if sum(lst)< num_steps:
#         for v in v_ts:
#             print(v_ts[v][_], v)
#         print(' ')
#
# for v in v_ts:
#     print(v_ts[v][:4], v)
# print('')
# for v in t_ts:
#     print(t_ts[v][:4], v)

column_urban = 0
'*******************************************************************'
Tvocabulary_size = len(dict_time);
Rvocabulary_size = len(dict_region);
Ovocabulary_size = len(dict_outlier)
batch_size = 64; n_input = 1; num_sampled = 128;
pretrain_flag = 0; save_flag = 1
embedding_size = 16; learning_rate = 0.001; top_k = 20;
num_neighbor = 8; attention_size = 32; state_size = int(sys.argv[4])
filter_size_layer = int(sys.argv[3]); mlp_layer = int(sys.argv[5])
filter_sizes = [2]*filter_size_layer
inner_dim2 = math.ceil(state_size/ 2 ** len(filter_sizes));
max_performance = [0.0] * 6; n_filter_layer = int(sys.argv[6])
####################################################################
graph = tf.Graph()
with graph.as_default():
    # Input data.
    input_oidx = tf.placeholder(tf.int32, shape=[None, 1])
    input_tidx = tf.placeholder(tf.int32, shape=[None, 1])
    input_ridx = tf.placeholder(tf.int32, shape=[None, 1])
    input_dseq = tf.placeholder(tf.int32, shape=[None, num_steps])
    input_wseq = tf.placeholder(tf.int32, shape=[None, num_steps])
    input_mseq = tf.placeholder(tf.int32, shape=[None, num_steps])
    input_wdseq = tf.placeholder(tf.int32, shape=[None, num_steps])
    input_mdseq = tf.placeholder(tf.int32, shape=[None, num_steps])
    input_mwseq = tf.placeholder(tf.int32, shape=[None, num_steps])
    output_c = tf.placeholder(tf.int32, [None, ])

    inputs_row = tf.placeholder(tf.int32)
    penalty_rate = tf.placeholder(tf.float32, name='penalty_rate')
    y = tf.reshape(output_c, [-1, 1])
    keep_rate = tf.placeholder(tf.float32)
    phase = tf.placeholder(tf.bool, name='phase')
    phase_RNN = tf.placeholder(tf.bool, name='phase_RNN')

    embed_d = tf.expand_dims(tf.expand_dims(my_GRU(input_dseq, inputs_row, 'd', phase_RNN),1),1)
    embed_w = tf.expand_dims(tf.expand_dims(my_GRU(input_wseq, inputs_row, 'w', phase_RNN),1),1)
    embed_m = tf.expand_dims(tf.expand_dims(my_GRU(input_mseq, inputs_row, 'm', phase_RNN),1),1)
    embed_wd = tf.expand_dims(tf.expand_dims(my_GRU(input_wdseq, inputs_row, 'wd', phase_RNN),1),1)
    embed_md = tf.expand_dims(tf.expand_dims(my_GRU(input_mdseq, inputs_row, 'md', phase_RNN),1),1)
    embed_mw = tf.expand_dims(tf.expand_dims(my_GRU(input_mwseq, inputs_row, 'mw', phase_RNN),1),1)


    multi_hidden_row1 = tf.concat([embed_d, embed_wd, embed_md], axis= 2)
    multi_hidden_row2 = tf.concat([embed_wd, embed_w, embed_mw], axis= 2)
    multi_hidden_row3 = tf.concat([embed_md, embed_mw, embed_m], axis= 2)
    multi_hidden = tf.concat([multi_hidden_row1, multi_hidden_row2, multi_hidden_row3], axis= 1)

    current_input_ce = CNN(multi_hidden, inputs_row, is_train=phase, keep_prob_cnn = keep_rate, str_name = 'whole')

    inputs = current_input_ce; input_dim = inner_dim2 * 9; output_dim = input_dim
    for i in range(mlp_layer):
        outputs = MLP(inputs, input_dim, output_dim, 'LL' + str(i))
        inputs = tf.nn.relu(outputs)
        input_dim = output_dim;
    out = MLP(inputs, input_dim, 2, 'Lout')
    pred = tf.nn.softmax(out)

    # lost = tf.reduce_mean(tf.multiply(tf.squared_difference(y, pred), y*4+1))
    # lost = tf.reduce_mean(tf.squared_difference(y, pred))
    lost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=tf.one_hot(output_c, 2), logits=out))
    cost = lost

    train_op = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(cost)

    pred_y = pred[:, 1]

    init = tf.global_variables_initializer()
####################################################################
config = tf.ConfigProto()
config.gpu_options.allow_growth = True
config.gpu_options.per_process_gpu_memory_fraction = 0.8
config.log_device_placement = True
config.allow_soft_placement = True
with tf.Session(graph=graph, config = config) as sess:
    if pretrain_flag == 0:
        sess.run(init)
    else:
        tf.train.Saver().restore(sess, save_file + '.ckpt')
        print('successful load pretrained parameters')
    average_loss = 0;
    average_cost = 0
    print('RUN')
    for epoch_idx, epoch in enumerate(gen_epochs(15, mtx_outlier_region)):
        for step, (batch_ts, batch_rs, batch_os, batch_ys) in enumerate(epoch):
            sequences = {k: [] for k in ['d', 'w', 'm', 'w_d', 'm_d', 'm_w']}
            for o, r, d in zip(batch_os, batch_rs, batch_ts):
                sequence = extract_feature(o, r, d)
                for k in sequence:
                    sequences[k].append(sequence[k])
            feed_dict = {input_dseq: np.array(sequences['d']), input_wseq: np.array(sequences['w']), \
                         input_mseq: np.array(sequences['m']), input_wdseq: np.array(sequences['w_d']), \
                         input_mdseq: np.array(sequences['m_d']), input_mwseq: np.array(sequences['m_w']), \
                         input_ridx: np.array(batch_rs).reshape(-1, 1), \
                         output_c: np.array(batch_ys), input_oidx: np.array(batch_os).reshape(-1, 1), \
                         inputs_row: batch_size, keep_rate: 0.5, phase: 1, penalty_rate: 0.0001, phase_RNN: 1}
            'calculate avg_loss'
            if save_flag == 1:
                _, loss_val, cost_val = sess.run([train_op, lost, cost], feed_dict=feed_dict)
            else:
                loss_val, cost_val = sess.run([lost, cost], feed_dict=feed_dict)
            average_loss += loss_val;
            average_cost += cost_val
            if step % 500 == 0:
                average_loss /= 500;
                average_cost /= 500
                # The average loss is an estimate of the loss over the last 2000 batches.
                print(epoch_idx, "Average loss at step ", step, ": ", average_loss, average_cost)
                my_pred = sess.run(pred_y, feed_dict=feed_dict)
                # print('Oembeddings', sess.run(Oembeddings, feed_dict=feed_dict)[:3])
                # print('input_embeddingT', sess.run(input_embeddingT, feed_dict=feed_dict))
                average_loss = 0;
                average_cost = 0

            if step % 500 == 0:
                save_file = 'results/result_311_mgrucnn_' + "_".join([str(_p) for _p in [bm, num_steps, filter_size_layer, state_size, mlp_layer]]) + '.txt'
                thefile = open(save_file, 'w')
                (performance, os, my_preds, ys) = run_eval(t_rs, t_os, t_ts, t_ys)
                if performance[2] >= max_performance[2]:
                    max_performance = performance
                    seperate_category(os, my_preds, ys)
                rslt = " & ".join([str(round(_p,4)) for _p in max_performance])
                thefile.write(str(epoch_idx) + ':' + rslt + "\n")